﻿using CommonLibraries.Guards;

namespace UserService.Api.ApplicationConfiguration
{
    public class RepositorySettings
    {
        public RepositorySettings(string connectionString)
        {
            ConnectionString = Check.NotNull(connectionString, nameof(connectionString));
        }

        public string ConnectionString { get; }
    }
}