﻿using CommonLibraries.Guards;
using UserService.UserContext.Infrastructure.Repositories.Settings;

namespace UserService.Api.ApplicationConfiguration
{
    public class ApplicationSettings
    {
        public ApplicationSettings(RepositorySettings repositorySettings, PhotoBlobRepositorySettings photoBlobRepositorySettings)
        {
            RepositorySettings = Check.NotNull(repositorySettings, nameof(repositorySettings));
            PhotoBlobRepositorySettings = Check.NotNull(photoBlobRepositorySettings, nameof(photoBlobRepositorySettings));
        }

        public PhotoBlobRepositorySettings PhotoBlobRepositorySettings { get; }
        
        public RepositorySettings RepositorySettings { get; }
    }
}