﻿using System.Threading.Tasks;
using CommonLibraries.Filters;
using CommonLibraries.Guards;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using UserService.UserContext.Application;

namespace UserService.Api.Controllers
{
    [Route("api/[controller]")]
    [TypeFilter(typeof(GlobalExceptionFilter))]
    public class UserController : Controller
    {
        public UserController(QueryUserService queryUserService, CommandUserService commandUserService)
        {
            _queryUserService = Check.NotNull(queryUserService, nameof(queryUserService));
            _commandUserService = Check.NotNull(commandUserService, nameof(commandUserService));
        }


        /// <summary>
        /// Returns user info.
        /// </summary>
        /// <param name="token">Access token.</param>
        /// <response code="200">Returns user info.</response>
        /// <response code="401">If token is invalid.</response>
        [Authorize(Roles = "Authorized, Pro")]
        [HttpGet("getByToken")]
        public async Task<IActionResult> GetByToken([FromHeader(Name = "token")] string token)
        {
            return Ok(await _queryUserService.GetByTokenAsync(token));
        }

        /// <summary>
        /// Uploads new user photo.
        /// </summary>
        /// <param name="userPhoto">User's photo form file.</param>
        /// <param name="token">User's access token.</param>
        /// <returns></returns>
        [Authorize(Roles = "Authorized, Pro")]
        [HttpPost("uploadUserPhoto")]
        public async Task<IActionResult> UploadUserPhoto([FromForm] IFormFile userPhoto, [FromHeader(Name = "token")] string token)
        {
            Check.NotNull(userPhoto, nameof(userPhoto));
            Check.NotNullOrEmpty(token, nameof(token));
            await _commandUserService.UploadUserPhoto(userPhoto.OpenReadStream(), token);
            return Ok();
        }
        
        
        /// <summary>
        /// Changes user information.
        /// </summary>
        /// <param name="name">User's name</param>
        /// <param name="surname">Surname's name</param>
        /// <returns></returns>
        [Authorize(Roles = "Authorized, Pro")]
        [HttpPatch("change-user-information")]
        public async Task<IActionResult> ChangeUserInformation([FromQuery(Name = "name")] string name, [FromQuery(Name = "surname")] string surname)
        {
            return Ok();
        }
        
        /// <summary>
        /// Changes user password.
        /// </summary>
        /// <param name="password">User's password</param>
        /// <returns></returns>
        [Authorize(Roles = "Authorized, Pro")]
        [HttpPatch("change-user-password")]
        public async Task<IActionResult> ChangeUserInformation([FromQuery(Name = "password")] string password)
        {
            return Ok();
        }
        

        private readonly CommandUserService _commandUserService;
        private readonly QueryUserService _queryUserService;
    }
}