using System;
using System.IO;
using System.Reflection;
using Azure.Storage.Blobs;
using CommonLibraries.Authentication;
using CommonLibraries.Crypto;
using CommonLibraries.Guards;
using CommonLibraries.Jwt;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Azure;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using RockLib.Configuration.ObjectFactory;
using UserService.Api.ApplicationConfiguration;
using UserService.Common.ApplicationConfiguration;
using UserService.UserContext.Application;
using UserService.UserContext.Infrastructure.Repositories;
using UserService.UserContext.Infrastructure.Repositories.Implementations;

namespace UserService.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            _configuration = Check.NotNull(configuration, nameof(configuration));
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddLogging();
            services.AddTrustedJwtAuthentication();

            services.ConfigureUserService(_configuration);

            services.AddSwaggerGen(options =>
            {
                var xmlFilename = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                options.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, xmlFilename));
            });
        }
        
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSwagger();
            app.UseSwaggerUI();
            
            app.UseRouting();
            
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }

        private readonly IConfiguration _configuration;
    }
}