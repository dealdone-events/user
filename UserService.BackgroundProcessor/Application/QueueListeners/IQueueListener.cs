﻿using System.Threading;
using System.Threading.Tasks;

namespace UserService.BackgroundProcessor.Application.QueueListeners
{
    public interface IQueueListener
    {
        public Task StartListeningAsync(CancellationToken cancellationToken);
    }
}