﻿using System;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using Azure.Messaging.ServiceBus;
using CommonLibraries.Guards;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using UserService.BackgroundProcessor.Application.Fabrics;
using UserService.BackgroundProcessor.Application.Providers;

namespace UserService.BackgroundProcessor.Application.QueueListeners
{
    public class AuthServiceQueueListener : IQueueListener
    {
        public AuthServiceQueueListener(IServiceBusClientFabric serviceBusClientFabric,
            IEventHandlerProvider eventHandlerProvider,
            IEventTypeProvider eventTypeProvider,
            ILogger<AuthServiceQueueListener> logger,
            string queueName)
        {
            _eventTypeProvider = Check.NotNull(eventTypeProvider, nameof(eventTypeProvider));
            _eventHandlerProvider = Check.NotNull(eventHandlerProvider, nameof(eventHandlerProvider));
            _serviceBusClientFabric = Check.NotNull(serviceBusClientFabric, nameof(serviceBusClientFabric));
            _logger = Check.NotNull(logger, nameof(logger));
            _queueName = Check.NotNullOrEmpty(queueName, nameof(queueName));
        }

        public async Task StartListeningAsync(CancellationToken cancellationToken)
        {
            var processor = _serviceBusClientFabric.CreateClient().CreateProcessor(_queueName);
            processor.ProcessMessageAsync += ProcessEventAsync;
            processor.ProcessErrorAsync += ProcessorOnProcessErrorAsync;
            await processor.StartProcessingAsync(cancellationToken);

            _logger.LogInformation("Start precessing auth messages");

            while (!cancellationToken.IsCancellationRequested)
            {
            }

            await processor.StopProcessingAsync(cancellationToken);
        }

        private Task ProcessorOnProcessErrorAsync(ProcessErrorEventArgs arg)
        {
            return Task.CompletedTask;
        }

        private async Task ProcessEventAsync(ProcessMessageEventArgs arg)
        {
            var jsonEvent = JsonDocument.Parse(arg.Message.Body.ToString()).RootElement;

            var eventTypeName = jsonEvent.GetProperty("Type").GetString();

            var eventType = _eventTypeProvider.GetEventTypeByEventTypeName(eventTypeName);

            var deserializedEvent = (dynamic) JsonConvert.DeserializeObject(arg.Message.Body.ToString(), eventType);
            var eventHandler = (dynamic) _eventHandlerProvider.GetEventHandlerByEventTypeName(eventTypeName);

            _logger.LogInformation("Start processing message: {MessageType}", eventType);

            try
            {
                await eventHandler.HandleAsync(deserializedEvent);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex,"Message handling failed with exception. Message: {Message}", arg.Message.Body.ToString());
                throw;
            }

            await arg.CompleteMessageAsync(arg.Message);
            
            _logger.LogInformation("End processing message: {MessageType}", eventType);
        }

        private readonly string _queueName;
        private readonly ILogger _logger;
        private readonly IEventHandlerProvider _eventHandlerProvider;
        private readonly IEventTypeProvider _eventTypeProvider;
        private readonly IServiceBusClientFabric _serviceBusClientFabric;
    }
}