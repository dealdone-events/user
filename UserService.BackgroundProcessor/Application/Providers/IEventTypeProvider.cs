﻿using System;

namespace UserService.BackgroundProcessor.Application.Providers
{
    public interface IEventTypeProvider
    {
        Type GetEventTypeByEventTypeName(string eventTypeName);
    }
}