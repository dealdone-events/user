﻿using System;
using System.Collections.Generic;
using System.Linq;
using CommonLibraries.Guards;
using Microsoft.Extensions.DependencyInjection;
using UserService.BackgroundProcessor.Application.EventHandlers;

namespace UserService.BackgroundProcessor.Application.Providers
{
    public class EventHandlerProvider : IEventHandlerProvider
    {
        public EventHandlerProvider(IServiceCollection serviceCollection)
        {
            Check.NotNull(serviceCollection, nameof(serviceCollection));
            _serviceProvider = serviceCollection.BuildServiceProvider();
            _eventHandlersMap = new Dictionary<string, Type>();

            foreach (var serviceDescription in serviceCollection)
            {
                if(serviceDescription.ServiceType.IsInterface)
                {
                    var @interface = serviceDescription.ServiceType; 
                    if (@interface.IsGenericType && @interface.GetGenericTypeDefinition() == typeof(IEventHandler<>))
                    {
                        _eventHandlersMap.Add(@interface.GetGenericArguments().First().ToString(),
                            serviceDescription.ServiceType);
                    }
                }
            }
        }

        public object GetEventHandlerByEventTypeName(string eventTypeName)
        {
            return _serviceProvider.GetService(_eventHandlersMap[eventTypeName]);
        }

        private readonly IServiceProvider _serviceProvider;
        private readonly IDictionary<string, Type> _eventHandlersMap;
    }
}