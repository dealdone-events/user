﻿namespace UserService.BackgroundProcessor.Application.Providers
{
    public interface IEventHandlerProvider
    {
        object GetEventHandlerByEventTypeName(string eventTypeName);
    }
}