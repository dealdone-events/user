﻿using System;

namespace UserService.BackgroundProcessor.Application.Providers
{
    public class EventTypeProvider : IEventTypeProvider
    {
        public Type GetEventTypeByEventTypeName(string eventTypeName)
        {
            var assemblies = AppDomain.CurrentDomain.GetAssemblies();
            
            Type eventType = null;

            foreach (var assembly in assemblies)
            {
                eventType = assembly.GetType(eventTypeName);
                
                if (eventType != null)
                {
                    break;
                }
            }

            if (eventType == null)
            {
                throw new ArgumentException("Unknown event type name");
            }

            return eventType;
        }
    }
}