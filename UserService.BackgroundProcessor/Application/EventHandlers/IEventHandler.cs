﻿using System.Threading.Tasks;

namespace UserService.BackgroundProcessor.Application.EventHandlers
{
    public interface IEventHandler<T>
        where T : class
    {
        public Task HandleAsync(T @event);
    }
}