﻿using System.Threading.Tasks;
using Auth.EventPublisher.Contracts.Events.UserService;
using CommonLibraries.Guards;
using UserService.UserContext.Application;

namespace UserService.BackgroundProcessor.Application.EventHandlers.AuthEventHandlers
{
    public class UserCreatedEventHandler : IEventHandler<UserCreated>
    {
        public UserCreatedEventHandler(CommandUserService commandUserService)
        {
            _commandUserService = Check.NotNull(commandUserService, nameof(commandUserService));
        }

        public async Task HandleAsync(UserCreated userCreatedEvent)
        {
            await _commandUserService.RegisterUserAsync(userCreatedEvent.Id, 
                userCreatedEvent.Email,
                userCreatedEvent.Name,
                userCreatedEvent.Surname,
                userCreatedEvent.Role,
                userCreatedEvent.PhotoUrl);
        }

        private readonly CommandUserService _commandUserService;
    }
}