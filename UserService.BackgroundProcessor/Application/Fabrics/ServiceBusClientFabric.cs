﻿using Azure.Messaging.ServiceBus;
using CommonLibraries.Guards;

namespace UserService.BackgroundProcessor.Application.Fabrics
{
    public class ServiceBusClientFabric : IServiceBusClientFabric
    {
        public ServiceBusClientFabric(string connectionString)
        {
            _connectionString = Check.NotNull(connectionString, nameof(connectionString));
        }

        public ServiceBusClient CreateClient()
        {
            return new ServiceBusClient(_connectionString);
        }

        private readonly string _connectionString;
    }
}