﻿using Azure.Messaging.ServiceBus;

namespace UserService.BackgroundProcessor.Application.Fabrics
{
    public interface IServiceBusClientFabric
    {
        public ServiceBusClient CreateClient();
    }
}