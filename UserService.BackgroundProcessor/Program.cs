using Auth.EventPublisher.Contracts.Events;
using Auth.EventPublisher.Contracts.Events.UserService;
using Azure.Storage.Blobs;
using CommonLibraries.Crypto;
using CommonLibraries.Jwt;
using Microsoft.Extensions.Azure;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using RockLib.Configuration.ObjectFactory;
using UserService.BackgroundProcessor.Application.EventHandlers;
using UserService.BackgroundProcessor.Application.EventHandlers.AuthEventHandlers;
using UserService.BackgroundProcessor.Application.Fabrics;
using UserService.BackgroundProcessor.Application.Providers;
using UserService.BackgroundProcessor.Application.QueueListeners;
using UserService.BackgroundProcessor.ApplicationConfiguration;
using UserService.Common.ApplicationConfiguration;
using UserService.UserContext.Application;
using UserService.UserContext.Infrastructure.Repositories;
using UserService.UserContext.Infrastructure.Repositories.Implementations;

namespace UserService.BackgroundProcessor
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        private static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureAppConfiguration(builder => { builder.AddJsonFile("appsettings.json"); })
                .ConfigureServices((hostContext, services) =>
                {
                    var configuration = hostContext.Configuration;
                    var settings = configuration.Create<ApplicationSettings>();

                    services.AddSingleton<IServiceBusClientFabric>(_ =>
                        new ServiceBusClientFabric(settings.AuthSettings.ServiceBusConnectionString));

                    services.AddSingleton<IQueueListener, AuthServiceQueueListener>(serviceProvider =>
                        new AuthServiceQueueListener(serviceProvider.GetService<IServiceBusClientFabric>(),
                            serviceProvider.GetService<IEventHandlerProvider>(),
                            serviceProvider.GetService<IEventTypeProvider>(),
                            serviceProvider.GetService<ILogger<AuthServiceQueueListener>>(),
                            settings.AuthSettings.QueueName));

                    services.AddSingleton<IEventHandlerProvider>(_ => new EventHandlerProvider(services));
                    services.AddSingleton<IEventTypeProvider, EventTypeProvider>();

                    services.AddScoped<IEventHandler<UserCreated>, UserCreatedEventHandler>();

                    services.ConfigureUserService(configuration);

                    services.AddHostedService<BackgroundProcessor>();
                })
                .ConfigureLogging(builder =>
                {
                    builder.ClearProviders();
                    builder.AddConsole();
                });
    }
}