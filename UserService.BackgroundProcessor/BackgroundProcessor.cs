using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using CommonLibraries.Guards;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using UserService.BackgroundProcessor.Application.QueueListeners;

namespace UserService.BackgroundProcessor
{
    public class BackgroundProcessor : BackgroundService
    {
        public BackgroundProcessor(ILogger<BackgroundProcessor> logger, IServiceProvider serviceProvider,
            IEnumerable<IQueueListener> queueListeners)
        {
            _queueListeners = Check.NotNull(queueListeners, nameof(queueListeners));
            _serviceProvider = Check.NotNull(serviceProvider, nameof(serviceProvider));
            _logger = Check.NotNull(logger, nameof(logger));
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            foreach (var queueListener in _queueListeners)
            {
                queueListener.StartListeningAsync(stoppingToken);
            }

            while (!stoppingToken.IsCancellationRequested)
            {
            }

            return Task.CompletedTask;
        }

        private readonly IEnumerable<IQueueListener> _queueListeners;
        private readonly IServiceProvider _serviceProvider;
        private readonly ILogger<BackgroundProcessor> _logger;
    }
}