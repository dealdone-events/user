﻿using CommonLibraries.Guards;
using UserService.UserContext.Infrastructure.Repositories.Settings;

namespace UserService.BackgroundProcessor.ApplicationConfiguration
{
    public class ApplicationSettings
    {
        public ApplicationSettings(AuthSettings authSettings, RepositorySettings repositorySettings, PhotoBlobRepositorySettings photoBlobRepositorySettings)
        {
            AuthSettings = Check.NotNull(authSettings, nameof(authSettings));
            RepositorySettings = Check.NotNull(repositorySettings, nameof(repositorySettings));
            PhotoBlobRepositorySettings = Check.NotNull(photoBlobRepositorySettings, nameof(photoBlobRepositorySettings));
        }

        public PhotoBlobRepositorySettings PhotoBlobRepositorySettings { get; }
        public RepositorySettings RepositorySettings { get; }
        
        public AuthSettings AuthSettings { get; }
    }
}