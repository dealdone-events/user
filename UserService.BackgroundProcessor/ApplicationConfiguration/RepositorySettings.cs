﻿using CommonLibraries.Guards;

namespace UserService.BackgroundProcessor.ApplicationConfiguration
{
    public class RepositorySettings
    {
        public RepositorySettings(string connectionString)
        {
            ConnectionString = Check.NotNullOrEmpty(connectionString, nameof(connectionString));
        }

        public string ConnectionString { get; }
    }
}