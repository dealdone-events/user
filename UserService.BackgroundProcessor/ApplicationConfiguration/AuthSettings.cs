﻿using CommonLibraries.Guards;

namespace UserService.BackgroundProcessor.ApplicationConfiguration
{
    public class AuthSettings
    {
        public AuthSettings(string serviceBusConnectionString, string queueName)
        {
            ServiceBusConnectionString =
                Check.NotNullOrEmpty(serviceBusConnectionString, nameof(serviceBusConnectionString));
            QueueName = Check.NotNullOrEmpty(queueName, nameof(queueName));
        }

        public string QueueName { get; }

        public string ServiceBusConnectionString { get; }
    }
}