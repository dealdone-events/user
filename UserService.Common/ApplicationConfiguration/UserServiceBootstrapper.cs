﻿using Azure.Storage.Blobs;
using CommonLibraries.Crypto;
using CommonLibraries.Jwt;
using Microsoft.Extensions.Azure;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using RockLib.Configuration.ObjectFactory;
using UserService.Api.ApplicationConfiguration;
using UserService.UserContext.Application;
using UserService.UserContext.Infrastructure.Repositories;
using UserService.UserContext.Infrastructure.Repositories.Implementations;

namespace UserService.Common.ApplicationConfiguration
{
    public static class UserServiceBootstrapper
    {
        public static IServiceCollection ConfigureUserService(this IServiceCollection services, IConfiguration configuration)
        {
            var settings = configuration.Create<ApplicationSettings>();

            services.AddAzureClients(builder => { builder.AddBlobServiceClient(settings.PhotoBlobRepositorySettings.ConnectionString); });

            services.AddScoped(services => new CommandUserService(
                services.GetService<IUserRepository>(),
                services.GetService<IUserPhotoRepository>(),
                services.GetService<BaseJwtService>(),
                settings.PhotoBlobRepositorySettings.BasePhotoUri));
            
            services.AddScoped<QueryUserService>();
            services.AddScoped<IUserRepository>(_ =>
                new UserRepository(settings.RepositorySettings.ConnectionString));
            services.AddScoped<IUserPhotoRepository>(serviceProvider =>
                new UserPhotoBlobRepository(settings.PhotoBlobRepositorySettings.BasePhotoUri,
                    settings.PhotoBlobRepositorySettings.ContainerName, serviceProvider.GetService<BlobServiceClient>()));

            services.AddSingleton<HashService>();
            services.AddSingleton<BaseJwtService>();

            return services;
        }
    }
}