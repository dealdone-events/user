﻿using System;
using System.Globalization;
using CommonLibraries.Guards;

namespace UserService.UserContext.Infrastructure.Repositories.Settings
{
    public class PhotoBlobRepositorySettings
    {
        public PhotoBlobRepositorySettings(Uri basePhotoUri, string blobPhotoHashSalt,
            string connectionString, string containerName)
        {
            BasePhotoUri = Check.NotNull(basePhotoUri, nameof(basePhotoUri));
            BlobPhotoHashSalt = Check.NotNull(blobPhotoHashSalt, nameof(blobPhotoHashSalt));
            ConnectionString = Check.NotNullOrEmpty(connectionString, nameof(connectionString));
            ContainerName = Check.NotNullOrEmpty(containerName, nameof(containerName));
        }

        public Uri BasePhotoUri { get; }
        
        public string ContainerName { get; }
        
        public string BlobPhotoHashSalt { get; }
        
        public string ConnectionString { get; }
    }
}