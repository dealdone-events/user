﻿using System;
using System.Threading.Tasks;
using CommonLibraries.Identities;
using UserService.UserContext.Domain;

namespace UserService.UserContext.Infrastructure.Repositories
{
    public interface IUserRepository
    {
        Task RegisterAsync(Guid id, string email, string name, string surname, UserRole role, Uri photoUrl);

        Task<User> GetById(Guid id);

        Task UpdatePhotoUrl(Guid id, Uri photoUrl);
    }
}