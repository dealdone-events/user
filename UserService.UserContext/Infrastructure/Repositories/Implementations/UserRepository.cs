﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;
using CommonLibraries.Guards;
using CommonLibraries.Identities;
using CommonLibraries.Repository;
using Microsoft.EntityFrameworkCore;
using UserService.UserContext.Domain;

namespace UserService.UserContext.Infrastructure.Repositories.Implementations
{
    public class UserRepository : DbContext, IUserRepository
    {
        private DbSet<User> Users { get; set; }

        public UserRepository(string connectionString)
        {
            _connectionString = Check.NotNull(connectionString, nameof(connectionString));
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>()
                .ToTable("user");

            modelBuilder.Entity<User>()
                .Property(e => e.Name)
                .HasColumnType("VARCHAR(255)");

            modelBuilder.Entity<User>()
                .Property(e => e.Surname)
                .HasColumnType("VARCHAR(255)");

            modelBuilder.Entity<User>()
                .Property(e => e.Email)
                .HasColumnType("VARCHAR(255)");
            
            modelBuilder.Entity<User>()
                .Property(e => e.Role)
                .HasColumnType("INT");

            modelBuilder.Entity<User>()
                .Property(e => e.PhotoUrl)
                .HasColumnType("VARCHAR(255)")
                .HasColumnName("photo_url");
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseMySql(_connectionString, ServerVersion.AutoDetect(_connectionString));
        }


        public async Task RegisterAsync(Guid id, string email, string name, string surname, UserRole role, Uri photoUrl)
        {
            if (await Users.AnyAsync(repositoryUser => repositoryUser.Id == id))
            {
                throw new RepositoryException("User already exists");
            }

            Users.Add(new User(id, email,name, surname, role, photoUrl));
            await SaveChangesAsync();
        }

        public async Task<User> GetById(Guid id)
        {
            var user = await Users.FindAsync(id);
            
            if (user == null)
            {
                throw new RepositoryException("User doesn't exists");
            }

            return user;
        }

        public async Task UpdatePhotoUrl(Guid id, Uri photoUrl)
        {
            var user = await GetById(id);
            user.UpdatePhotoUrl(photoUrl);
            Users.Update(user);
            await SaveChangesAsync();
        }

        private readonly string _connectionString;
    }
}