﻿using System;
using System.Globalization;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Azure.Storage.Blobs;
using Azure.Storage.Blobs.Models;
using CommonLibraries.Crypto;
using CommonLibraries.Guards;
using CommonLibraries.Repository;
using UserService.UserContext.Infrastructure.Repositories.Settings;

namespace UserService.UserContext.Infrastructure.Repositories.Implementations
{
    public class UserPhotoBlobRepository : IUserPhotoRepository
    {
        public UserPhotoBlobRepository(Uri basePhotoUrl, string containerName, BlobServiceClient blobServiceClient)
        {
            _basePhotoUrl = Check.NotNull(basePhotoUrl, nameof(basePhotoUrl));
            _blobServiceClient = Check.NotNull(blobServiceClient, nameof(blobServiceClient));
            _containerName = Check.NotNullOrEmpty(containerName, nameof(containerName));
        }
        
        public async Task<Uri> UploadPhoto(Stream fileStream, Guid userId)
        {
            var blobClient = GetBlobClient(userId);
            await blobClient.DeleteIfExistsAsync(DeleteSnapshotsOption.IncludeSnapshots, new BlobRequestConditions());
            await blobClient.UploadAsync(fileStream);
            return new Uri(_basePhotoUrl + blobClient.Name);
        }

        public async Task<Stream> GetPhotoByPath(string path)
        {
            var blobContainerClient = _blobServiceClient.GetBlobContainerClient(_containerName);
            var blobClient = blobContainerClient.GetBlobClient(path);
            if (!(await blobClient.ExistsAsync()).Value)
            {
                throw new RepositoryException("Photo was not found");
            }

            var stream = (await blobClient.DownloadAsync()).Value.Content;
            var memoryStream = new MemoryStream();
            await stream.CopyToAsync(memoryStream);
            memoryStream.Seek(0, SeekOrigin.Begin);

            return memoryStream;
        }

        private BlobClient GetBlobClient(Guid userId)
        {
            var blobContainerClient = _blobServiceClient.GetBlobContainerClient(_containerName);
            var fileName = Convert.ToBase64String(Encoding.UTF8.GetBytes(userId.ToString()));
            return blobContainerClient.GetBlobClient(fileName);
        }

        private readonly string _containerName;
        private readonly Uri _basePhotoUrl;
        private readonly BlobServiceClient _blobServiceClient;
    }
}