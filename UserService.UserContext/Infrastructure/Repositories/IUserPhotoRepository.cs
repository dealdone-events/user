﻿using System;
using System.IO;
using System.Threading.Tasks;

namespace UserService.UserContext.Infrastructure.Repositories
{
    public interface IUserPhotoRepository
    {
        Task<Uri> UploadPhoto(Stream fileStream, Guid userId);

        Task<Stream> GetPhotoByPath(string path);
    }
}