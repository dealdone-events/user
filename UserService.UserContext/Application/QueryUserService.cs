﻿using System.IO;
using System.Threading.Tasks;
using CommonLibraries.Guards;
using CommonLibraries.Jwt;
using UserService.UserContext.Application.Extensions;
using UserService.UserContext.Domain;
using UserService.UserContext.Infrastructure.Repositories;

namespace UserService.UserContext.Application
{
    public class QueryUserService
    {
        public QueryUserService(BaseJwtService jwtService,IUserRepository userRepository, IUserPhotoRepository userPhotoRepository)
        {
            _jwtService = Check.NotNull(jwtService, nameof(jwtService));
            _userRepository = Check.NotNull(userRepository, nameof(userRepository));
            _userPhotoRepository = Check.NotNull(userPhotoRepository, nameof(userPhotoRepository));
        }

        public async Task<User> GetByTokenAsync(string token)
        {
            var userJwtModel = _jwtService.ParseUser(token);
            return await _userRepository.GetById(userJwtModel.Id.Value);
        }

        public async Task<(Stream, string)> GetPhoto(string path)
        {
            var stream = await _userPhotoRepository.GetPhotoByPath(path);
            stream.IsImage(out var type);
            return (stream, type);
        }

        private readonly BaseJwtService _jwtService;
        private readonly IUserPhotoRepository _userPhotoRepository;
        private readonly IUserRepository _userRepository;
    }
}