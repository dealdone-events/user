﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace UserService.UserContext.Application.Extensions
{
    public static class ImgExtension
    {
        static ImgExtension()
        {
            ImageTypes = new Dictionary<string, string>
            {
                { "FFD8", "jpeg" },
                { "89504E470D0A1A0A", "png" }
            };
        }

        public static bool IsImage(this Stream stream, out string type)
        {
            stream.Seek(0, SeekOrigin.Begin);
            StringBuilder builder = new StringBuilder();
            int largestByteHeader = ImageTypes.Max(img => img.Key.Length);
        
            for (int i = 0; i < largestByteHeader; i++)
            {
                string bit = stream.ReadByte().ToString("X2");
                builder.Append(bit);
            
                string builtHex = builder.ToString();
                bool isImage = ImageTypes.Keys.Any(img => img == builtHex);
                if (isImage)
                {
                    stream.Seek(0, SeekOrigin.Begin);
                    type = ImageTypes[builtHex];
                    return true;
                }
            }
            stream.Seek(0, SeekOrigin.Begin);
            type = null;
            return false; 
        }

        private static readonly Dictionary<string, string> ImageTypes;
    }
}