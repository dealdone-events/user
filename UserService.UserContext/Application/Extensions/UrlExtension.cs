﻿using System;
using Newtonsoft.Json;

namespace UserService.UserContext.Application.Extensions
{
    public static class UrlExtension
    {
        public static bool IsBlank(this Uri uri)
        {
            return uri.ToString() == "about:blank";
        }
    }
}