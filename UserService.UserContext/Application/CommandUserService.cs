﻿using System;
using System.IO;
using System.Threading.Tasks;
using CommonLibraries.Guards;
using CommonLibraries.Identities;
using CommonLibraries.Jwt;
using UserService.UserContext.Application.Extensions;
using UserService.UserContext.Infrastructure.Repositories;

namespace UserService.UserContext.Application
{
    public class CommandUserService
    {
        public CommandUserService(IUserRepository userRepository, IUserPhotoRepository userPhotoRepository, BaseJwtService baseJwtService, Uri basePhotoUrl)
        {
            _userRepository = Check.NotNull(userRepository, nameof(userRepository));
            _userPhotoRepository = Check.NotNull(userPhotoRepository, nameof(userPhotoRepository));
            _baseJwtService = Check.NotNull(baseJwtService, nameof(baseJwtService));
            _basePhotoUrl = Check.NotNull(basePhotoUrl, nameof(basePhotoUrl));
        }

        public async Task RegisterUserAsync(Guid id, string email, string name, string surname, UserRole role, Uri photoUrl)
        {
            Check.NotNullOrEmpty(email, nameof(email));
            Check.NotNullOrEmpty(name, nameof(name));
            Check.NotNull(photoUrl, nameof(photoUrl));
            
            if (photoUrl.IsBlank())
            {
                photoUrl = new Uri(_basePhotoUrl + "default");
            }
            await _userRepository.RegisterAsync(id, email, name, surname, role, photoUrl);
        }

        public async Task UploadUserPhoto(Stream fileStream, string token)
        {
            Check.NotNull(fileStream, nameof(fileStream));
            Check.NotNullOrEmpty(token, nameof(token));
            if (!fileStream.IsImage(out string type))
            {
                throw new ArgumentException("This is not jpg, jpeg or png");
            }
            
            var userJwtModel = _baseJwtService.ParseUser(token);
            var photoUrl = await _userPhotoRepository.UploadPhoto(fileStream, userJwtModel.Id.Value);
            await _userRepository.UpdatePhotoUrl(userJwtModel.Id.Value, photoUrl);
        }

        private readonly BaseJwtService _baseJwtService;
        private readonly IUserPhotoRepository _userPhotoRepository;
        private readonly IUserRepository _userRepository;
        private readonly Uri _basePhotoUrl;
    }
}