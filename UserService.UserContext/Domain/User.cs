﻿using System;
using System.ComponentModel.DataAnnotations;
using CommonLibraries.Guards;
using CommonLibraries.Identities;

namespace UserService.UserContext.Domain
{
    public class User
    {
        private User()
        {
        }

        public User(Guid id,
            string email,
            string name,
            string surname,
            UserRole role,
            Uri photoUrl)
        {
            Name = Check.NotNullOrEmpty(name, nameof(name));
            Email = Check.NotNullOrEmpty(email, nameof(email));
            Surname = Check.NotNull(surname, nameof(surname));
            Id = id;
            Role = role;
            PhotoUrl = Check.NotNull(photoUrl, nameof(photoUrl));
        }

        public void UpdatePhotoUrl(Uri photoUrl)
        {
            PhotoUrl = Check.NotNull(photoUrl, nameof(photoUrl));
        }

        [Key] public Guid Id { get; private set; }

        public string Name { get; }

        public string Surname { get; }

        public string Email { get; }

        public UserRole Role { get; }

        public Uri PhotoUrl { get; private set; }
    }
}