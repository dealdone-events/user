﻿using System.IO;
using System.Threading.Tasks;
using CommonLibraries.Filters;
using CommonLibraries.Guards;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualBasic;
using UserService.UserContext.Application;

namespace UserService.Storage.Controllers
{
    [Route("[controller]")]
    [TypeFilter(typeof(GlobalExceptionFilter))]
    public class PhotoStorageController : Controller
    {
        public PhotoStorageController(QueryUserService queryUserService)
        {
            _queryUserService = Check.NotNull(queryUserService, nameof(queryUserService));
        }

        [HttpGet("{path}")]
        public async Task<IActionResult> GetPhoto(string path)
        {
            Check.NotNullOrEmpty(path, nameof(path));
            var (file, type) = await _queryUserService.GetPhoto(path);
            return File(file, $"image/{type}");
        }

        private readonly QueryUserService _queryUserService;
    }
}