using CommonLibraries.Guards;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using UserService.Common.ApplicationConfiguration;

namespace UserService.Storage
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            _configuration = Check.NotNull(configuration, nameof(configuration));
        }

        
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            services.ConfigureUserService(_configuration);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }

        private readonly IConfiguration _configuration;
    }
}